const gulp = require('gulp');
const webpack = require('webpack-stream');
const jp = require('jsonpath');
const fs = require('fs');
const download = require("gulp-download-files");
const path = require('path');
const replace = require('gulp-replace');

gulp.task('js', function () {
    return gulp.src(['app/*.js', 'app/*.scss'])
        .pipe(webpack({
            mode: (process.env.NODE_ENV || false) ? 'production' : 'development',
            module: {
                rules: [
                    {
                        test: /\.s[ac]ss$/i,
                        use: [
                            // Creates `style` nodes from JS strings
                            "style-loader",
                            // Translates CSS into CommonJS
                            "css-loader",
                            // Compiles Sass to CSS
                            "sass-loader",
                        ],
                    },
                ]
            },
        }))
        .pipe(gulp.dest('./dist/js'))
})

gulp.task('styles', function () {
    return gulp.src(['app/*.css'])
        .pipe(gulp.dest('dist'));
})

gulp.task('assets', function () {
    return gulp.src(['app/assets/**/*.*'])
        .pipe(gulp.dest('dist/assets'));
})

gulp.task('media', function () {
    return gulp.src(['media/**/*.*'])
        .pipe(gulp.dest('dist/media'));
})


gulp.task('html', function () {
    return gulp.src('app/*.html')
        .pipe(replace(/\{\{CI_COMMIT_SHORT_SHA\}\}/, process.env.CI_COMMIT_SHORT_SHA))
        .pipe(gulp.dest('dist'));
})

gulp.task('data', function () {
    const artefacts = JSON.parse(fs.readFileSync('data/artefacts.json'));
    const missionDocumentUrls = jp.query(artefacts, "$.*.groups.*");
    missionDocumentUrls.forEach(url => {
        let filename = (new URL(url)).pathname
        download(url)
            .pipe(gulp.dest(path.join("dist", "data", path.dirname(filename))));
    });
    return gulp.src(['data/*.json', 'data/*.yaml'])
        .pipe(gulp.dest('dist/data'));
})

gulp.task('default', gulp.series('js', 'styles', 'assets', 'media', 'html', 'data'));

gulp.task('watch', function () {
    gulp.watch('data/*', gulp.series(['data']));
    gulp.watch('app/*', gulp.series(['default']));
});
