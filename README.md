# Gaia-X Framework

Visual representation of the Gaia-X Framework.

## For developers

### Data

All the artefacts info are in `data/artefacts.json`.

To avoid CORS issues, YAML files of the mission documents are prefetch in `gulp data` task (part of the default task).

### local development

After `npm install`, run `gulp watch` and load the `dist/index.html` file in your browser.
