const steps = [
    // -------------- GENERIC --------------
    {
        title: "Hello 👋",
        intro: "Welcome in this quick tour of the <b>Gaia-X Framework</b> !<br>You can use the ⬅️ and ➡️ from your keyboard.<hr><b>Best experience on a FullHD screen in full screen (F11)</b>",
    },
    {
        element: document.querySelector('div.grid'),
        title: "Gaia-X Framework",
        intro: `This is a matrix representation of all the Gaia-X artefacts and how they relate to each others.<br>`,
        onchange: function(d3) {
            d3.selectAll("input[type=checkbox]").property("checked", false).dispatch('change');
            d3.selectAll('#toggles button').classed('accordion_active', true).dispatch("click"); // deactivate all toggles
        },
    },
    // -------------- ARTEFACTS --------------
    {
        element: document.querySelector('div.Specifications'),
        title: "Gaia-X deliverables 1/3",
        intro: `There are 3 types of <b>Gaia-X Specifications📄</b>:<ul>
        <li>Functional specifications</li>
        <li>Technical specifications</li>
        <li>Rules specifications</li>
        </ul><br><img src="media/deliverables.png" width=250px">`,
        onchange: function(d3) {
            d3.selectAll("input[type=checkbox]").property("checked", false);
            d3.select("input#Functional-Specifications").property("checked", true);
            d3.select("input#Technical-Specifications").property("checked", true);
            d3.select("input#Rules-Specifications").property("checked", true);
            d3.selectAll("input[type=checkbox]").dispatch("change");
        },
    },
    {
        element: document.querySelector('div.Labels'),
        title: "Gaia-X deliverables 2/3",
        intro: `We have a <b>Label framework⚖️</b> where Gaia-X defines its Gaia-X Label.<br>
        Ecosystems can define their own domain or regulation specific labels too<br>.
        <img src="media/deliverables.png" width=250px">`,
        onchange: function(d3) {
            document.querySelector("div.Labels").style.setProperty('z-index', "-1", "important") // hack to avoid Intro.js placing background div above artefact divs
            d3.selectAll("input[type=checkbox]").property("checked", false);
            d3.select("input#Labels").property("checked", true);
            d3.selectAll("input[type=checkbox]").dispatch("change");
        },
    },
    {
        element: document.querySelector('div.Software'),
        title: "Gaia-X deliverables 3/3",
        intro: `We have <b>open source implementation</b> for Services⚙️ and Tools📦 to operationalise Gaia-X.<br><img src="media/deliverables.png" width=250px">`,
        onchange: function(d3) {
            document.querySelector("div.Software").style.setProperty('z-index', "-1", "important") // hack to avoid Intro.js placing background div above artefact divs
            d3.selectAll("input[type=checkbox]").property("checked", false);
            d3.select("input#Software").property("checked", true);
            d3.selectAll("input[type=checkbox]").dispatch("change");
        },
    },
    {
        element: document.querySelector('div.Gaia-X-Compliance'),
        title: "Gaia-X Compliance",
        intro: `Running instance⚙️ of the Gaia-X Compliance rules implementation as defined in the Gaia-X Trust Framework document.<br> There can be <a href="https://gaia-x.eu/news/latest-news/gaia-x-compliance-service-deployment-scenario/">several implementation</a> as long as the instances are operated according to the governance defined by the Gaia-X association.`,
        onchange: function(d3) {
            document.querySelector("div.Gaia-X-Compliance").style.setProperty('z-index', "-1", "important") // hack to avoid Intro.js placing background div above artefact divs
            d3.selectAll("input[type=checkbox]").property("checked", false);
            d3.select("input#Gaia-X-Compliance").property("checked", true);
            d3.selectAll("input[type=checkbox]").dispatch("change");
        },
    },
    {
        element: document.querySelector('div.GXFS-Toolbox'),
        title: "Gaia-X Federation Services toobox",
        intro: `Optional set of software products📦 to build federations.`,
        onchange: function(d3) {
            document.querySelector("div.GXFS-Toolbox").style.setProperty('z-index', "-1", "important") // hack to avoid Intro.js placing background div above artefact divs
            d3.selectAll("input[type=checkbox]").property("checked", false);
            d3.select("input#GXFS-Toolbox").property("checked", true);
            d3.selectAll("input[type=checkbox]").dispatch("change");
        },
    },
    // -------------- SCOPES --------------
    {
        element: document.querySelector('div.Compliance'),
        title: "Gaia-X scopes 1/3",
        intro: `The <b>Gaia-X Compliance</b> for a common digital governance based on European values.<br><img src="media/scopes.png" width=250px">`,
        onchange: function(d3) {
            document.querySelector("div.Compliance").style.setProperty('z-index', "-1", "important") // hack to avoid Intro.js placing background div above artefact divs
            d3.selectAll("input[type=checkbox]").property("checked", false);
            d3.select("input#Compliance").property("checked", true);
            d3.selectAll("input[type=checkbox]").dispatch("change");
        },
    },
    {
        element: document.querySelector('div.Federation'),
        title: "Gaia-X scopes 2/3",
        intro: `A mean for interoperability across <b>federated ecosystems</b>.<br><img src="media/scopes.png" width=250px">`,
        onchange: function(d3) {
            document.querySelector("div.Federation").style.setProperty('z-index', "-1", "important") // hack to avoid Intro.js placing background div above artefact divs
            d3.selectAll("input[type=checkbox]").property("checked", false);
            d3.select("input#Federation").property("checked", true);
            d3.selectAll("input[type=checkbox]").dispatch("change");
        },
    },
    {
        element: document.querySelector('div.Data-Exchange'),
        title: "Gaia-X scopes 3/3",
        intro: `A mean to perform <b>data exchange</b> and anchor data contract negotiation results into the infrastructure.<br><img src="media/scopes.png" width=250px">`,
        onchange: function(d3) {
            document.querySelector("div.Data-Exchange").style.setProperty('z-index', "-1", "important") // hack to avoid Intro.js placing background div above artefact divs
            d3.selectAll("input[type=checkbox]").property("checked", false);
            d3.select("input#Data-Exchange").property("checked", true);
            d3.selectAll("input[type=checkbox]").dispatch("change");
        },
    },
    // -------------- FILTERS --------------
    {
        element: document.querySelector('div#selectors'),
        title: "Filters",
        intro: "Use this panel to configure the view on the left.<br>Try to check one of the checkboxes.",
        onchange: function(d3) {
            d3.selectAll("input[type=checkbox]").property("checked", false);
        },
    },
    {
        element: document.querySelector('div#toggles'),
        title: "Toggles",
        intro: 'Ex: Use toggles like <span class="accordion_title">Show/hide Release</span> to show additional information for each artefact.',
    },
    {
        element: document.querySelector('div.Architecture-Document'),
        title: "Extra info per artefact 1/3",
        intro: `For each artefact, you have access to:
        <ul>
        <li>latest version</li>
        <li>next draft version</li>
        </ul>`,
        onchange: function(d3) {
            d3.selectAll('#toggles button').classed('accordion_active', true).dispatch("click"); // deactivate all toggles
            d3.select('button[data-panel=release]').dispatch("click");
        },
    },
    {
        element: document.querySelector('div.Architecture-Document'),
        title: "Extra info per artefact 2/3",
        intro: `The associated Gaia-X Working Goups open to all Gaia-X members.`,
        onchange: function(d3) {
            d3.selectAll('#toggles button').classed('accordion_active', true).dispatch("click"); // deactivate all toggles
            d3.select('button[data-panel=group]').dispatch("click");
        },
    },
    {
        element: document.querySelector('div.Architecture-Document'),
        title: "Extra info per artefact 3/3",
        intro: `The list of main sections in the document.`,
        onchange: function(d3) {
            d3.selectAll('#toggles button').classed('accordion_active', true).dispatch("click"); // deactivate all toggles
            d3.select('button[data-panel=section]').dispatch("click");
        },
    },
    {
        title: "Thank you ! 💕",
        intro: 'Your turn to browse this first knowledge base.<br>If you have any question, please reach out to <a href="mailto:info@gaia-x.eu">info@gaia-x.eu</a> or <a href="https://gitlab.com/gaia-x/technical-committee/framework" target="_blank">Gitlab</a>',
        onchange: function(d3) {
            d3.selectAll("input[type=checkbox]").property("checked", false).dispatch('change');
            d3.selectAll('#toggles button').classed('accordion_active', true).dispatch("click"); // deactivate all toggles
        },
    }
]

export { steps };
