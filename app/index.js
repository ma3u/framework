import * as d3 from 'd3';
import yaml from 'js-yaml';
import * as introJs from 'intro.js/intro.js';

import "./style.scss";
import { steps } from './tour_steps.js';


function isEmptyObject(obj) {
    return (
        Object.getPrototypeOf(obj) === Object.prototype &&
        Object.getOwnPropertyNames(obj).length === 0 &&
        Object.getOwnPropertySymbols(obj).length === 0
    ) || (
            Object.getPrototypeOf(obj) === Array.prototype &&
            obj.length === 0
        );
}

d3.selectAll("div#selectors input[type=checkbox]").on("change", function () {
    let checked = d3.select(this).property("checked");
    let name = d3.select(this).attr("id");
    let className = d3.select(this).attr("data-selclass");
    console.log(name, checked);
    d3.select(`div.${name}`).classed(className, checked);
    // d3.select(`div.${name}`).style("display", checked ? "hidden" : "block");
    if (checked) {
        d3.select(`div.${name}`).append("h3").text(name);
    } else {
        d3.select(`div.${name} > h3`).remove();
    }
});

d3.selectAll("div#filters input[type=checkbox]").on("change", function () {
    let checked = d3.select(this).property("checked");
    let value = d3.select(this).attr("value");
    let className = d3.select(this).attr("data-selclass");
    console.log(value, checked);
    d3.selectAll(`div[${value}]`).classed(className, checked);
});

d3.select("button#generateDirectLink").on("click", function () {
    let direct_url = new URL(window.document.URL)
    direct_url.search = ""
    d3.selectAll("div#selectors input[type=checkbox]").each(function () {
        if (d3.select(this).property("checked")) {
            direct_url.searchParams.set(this.id, "1")
        }
    });
    window.location = direct_url.href
});

d3.select("#startTour").on("click", function () {
    const intro = introJs();
    intro
        .setOptions({ steps: steps })
        .onbeforechange(function () {
            // check to see if there is a function on this step
            if (this._introItems[this._currentStep] && this._introItems[this._currentStep].onbeforechange) {
                //if so, execute it.
                this._introItems[this._currentStep].onbeforechange(d3);
            }
        })
        .onchange(function () {  //intro.js built in onchange function
            if (this._introItems[this._currentStep] && this._introItems[this._currentStep].onchange) {
                this._introItems[this._currentStep].onchange(d3);
            }
        })
        .start();
});

function loadQueryParameters(init) {
    d3.selectAll("input[type=checkbox]").property("checked", false);
    let params = new URLSearchParams(init);
    if (params.toString() === "") {
        params = new URLSearchParams({ "Specifications": 1, "Software": 1, "Labels": 1 });
    }
    for (const [key, value] of params.entries()) {
        d3.select(`input#${key}`).property("checked", true);
    };
    d3.selectAll("input[type=checkbox]").dispatch("change");
}

(async function () {
    const artefacts = await d3.json("data/artefacts.json");

    for (const name in artefacts) {
        console.log(`Artefact: ${name}`);
        let artefactElt = d3.select(`div.${name}`)
        artefactElt.attr("data-mandatory", artefacts[name].mandatory || false);
        artefactElt.attr("data-artefact", true);
        artefactElt.append("span").classed("title", true).text(artefacts[name].name || name);
        if (artefacts[name].releases) {
            addArtefactReleases(artefactElt, artefacts[name].releases); // insert release information
        }
        addArtefactWorkingGroup(artefactElt, artefacts[name].groups || []); // insert working group info
        addArtefactSections(artefactElt, artefacts[name].sections || []); // insert document sections
    }
    animateAccordions();
    loadQueryParameters(new URL(window.document.URL).search);
})();

function capitalize(word) {
    const lower = word.toLowerCase();
    return word.charAt(0).toUpperCase() + lower.slice(1);
}


function addArtefactReleases(artefactElt, releases) {
    let panel = artefactElt.append("fieldset").attr("data-panel", "release").classed("accordion_panel", true)
    panel.append("legend").text("Release(s)").attr("title", 'Show/Hide info using the toggle in the left menu');
    let ul = panel.insert("div").insert("ul");
    if (isEmptyObject(releases)) {
        ul.insert("li").style("font-style", "italic").text("No release yet.");
    } else {
        for (const releaseOrder of ["latest", "next"]) {
            if (!releases.hasOwnProperty(releaseOrder)) {
                continue;
            }
            let releaseInfo = ul.insert("li").text(capitalize(releaseOrder) + ':')
            releases[releaseOrder].links.forEach(link => { // .links
                releaseInfo.append("a").attr("target", "_blank").attr("href", link).text(`(${releases[releaseOrder].version})`);
            });

        }
    }
}

function addArtefactWorkingGroup(artefactElt, groups) {
    let panel = artefactElt.append("fieldset").attr("data-panel", "group").classed("accordion_panel", true)
    panel.append("legend").text("Group(s)").attr("title", 'Show/Hide info using the toggle in the left menu');
    let ul = panel.insert("div").insert("ul");
    groups.forEach(async link => {
        // convert raw URL to local URL prefetch during gulp data task
        const externalUrl = new URL(link);
        const localUrl = "data" + externalUrl.pathname;
        const missionDocumentYaml = await fetch(localUrl);
        let response = await missionDocumentYaml.blob();
        const doc = yaml.load(await response.text());
        let name = `${doc.name} ${doc.type}`
        ul.insert("li").append("a").attr("target", "_blank").attr("href", link).text(`${name}`);
    });
}

function addArtefactSections(artefactElt, sections) {
    let panel = artefactElt.append("fieldset").attr("data-panel", "section").classed("accordion_panel", true)
    panel.append("legend").text("Section(s)").attr("title", 'Show/Hide info using the toggle in the left menu');
    let ul = panel.insert("div").insert("ul");
    sections.forEach(element => {
        ul.insert("li").html(element)
    });
}

function animateAccordions() {
    d3.selectAll("button.accordion_title").on("click", function () {
        this.classList.toggle("accordion_active");
        let panelType = d3.select(this).attr("data-panel");
        let activated = d3.select(this).classed("accordion_active")
        console.log(panelType, activated)
        d3.selectAll(`fieldset.accordion_panel[data-panel=${panelType}]`).each(function () {
            let panel = this;
            let legend = d3.select(panel).select("legend")
            if (!activated) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = `calc(${panel.scrollHeight}px + ${legend.style("height")})`;
            }
        });
    });
}
// d3.selectAll("div")
//     .data(data, function (d) { return (d && d.key) || d3.select(this).attr("id"); })
//     .text(function (d) { return d.val; });
